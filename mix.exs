defmodule ActivityPubClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :activity_pub_client,
      name: "ActivityPubClient",
      description: "An ActivityPub client library",
      source_url: "https://gitlab.com/technostructures/kazarma/activity_pub_client",
      homepage_url: "https://gitlab.com/technostructures/kazarma/activity_pub_client",
      package: [
        licenses: ["LGPL-3.0"],
        links: %{"GitLab" => "https://gitlab.com/technostructures/kazarma/activity_pub_client"}
      ],
      docs: [
        extras: ["README.md"],
        main: "readme"
      ],
      version: "0.1.2",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps()
      # dialyzer: [
      # plt_add_deps: [:ex_unit],
      # plt_file: {:no_warn, "priv/plts/dialyzer.plt"},
      # ignore_warnings: ".dialyzer_ignore.exs"
      # ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {ActivityPubClient.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:ecto_sql, "~> 3.7"},
      {:oauth2, "~> 2.0"},
      {:jason, "~> 1.2"},
      {:postgrex, ">= 0.15.10"},
      {:meck, "~> 0.9.2", only: :test},
      {:mock, "~> 0.3.0", only: :test},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      # {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false},
      {:gun, "~> 1.3"}
    ]
  end
end
