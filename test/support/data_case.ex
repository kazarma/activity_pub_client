defmodule ActivityPubClient.DataCase do
  @moduledoc """
  This module defines the setup for tests requiring
  access to the application's data layer.

  You may define functions here to be used as helpers in
  your tests.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use ActivityPubClient.DataCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      alias ActivityPubClient.Repo

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import ActivityPubClient.DataCase
    end
  end

  setup tags do
    {:ok, _} = Ecto.Adapters.Postgres.ensure_all_started(ActivityPubClient.Repo, :temporary)

    # This cleans up the test database and loads the schema
    # Mix.Task.run("ecto.drop")
    # Mix.Task.run("ecto.create")
    # Mix.Task.run("ecto.load")

    # Start a process ONLY for our test run.
    # {:ok, _pid} = ActivityPubClient.Repo.start_link()

    :ok = Ecto.Adapters.SQL.Sandbox.checkout(ActivityPubClient.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(ActivityPubClient.Repo, {:shared, self()})
    end

    :ok
  end

  @doc """
  A helper that transforms changeset errors into a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end
end
