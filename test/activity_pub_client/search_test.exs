defmodule ActivityPubClient.SearchTest do
  use ExUnit.Case

  import ActivityPubClient.Search
  import Mock

  describe "search/2" do
    test "sends the corresponding request" do
      with_mock OAuth2.Client,
        get: fn :client, "/api/v2/search?q=bob" -> {:ok, %OAuth2.Response{body: :resp}} end do
        assert {:ok, :resp} == search(:client, "bob")
      end
    end
  end
end
