defmodule ActivityPubClient.Status do
  @moduledoc """
  Requests to the status (toot) API in Mastodon, also implemented in Pleroma.

  Reference: https://docs.joinmastodon.org/methods/statuses/
  """

  @doc """
  Posts a status (toot).
  Implemented by Mastodon and Pleroma.

  Expects a `%ActivityPubClient.Client{}` struct, the status content and a
  keyword list of optional options. Options can be:
  - media_ids: List of Attachment ids to be attached as media
  - in_reply_to_id: ID of the status being replied to, if status is a reply
  - sensitive: Mark status and attached media as sensitive? (boolean)
  - spoiler_text: Text to be shown as a warning or subject before the actual
    content
  - visibility: Visibility of the posted status. Enumerable oneOf public,
    unlisted, private, direct
  - scheduled_at: ISO 8601 Datetime at which to schedule a status
  - language: ISO 639 language code for this status
  """
  def post_status(
        client,
        content,
        opts \\ []
      ) do
    body = Keyword.put(opts, :status, content) |> Enum.into(%{})

    case OAuth2.Client.post(client, "/api/v1/statuses", body, [
           {"Content-Type", "application/json"}
         ]) do
      {:ok, %OAuth2.Response{body: resp}} ->
        {:ok, resp}

      {:error, %OAuth2.Response{status_code: 401, body: _body}} ->
        {:error, :unauthorized}

      {:error, %OAuth2.Error{reason: reason}} ->
        {:error, inspect(reason)}
    end
  end
end
