defmodule ActivityPubClient.App do
  @moduledoc """
  Ecto schema to keep track of registered apps on ActivityPub servers.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "ap_client_apps" do
    field(:site, :string)
    field(:client_id, :string)
    field(:client_secret, :string)
    # field(:redirect_uri, :string)

    timestamps()
  end

  @doc false
  def changeset(app, attrs) do
    app
    |> cast(attrs, [:site, :client_id, :client_secret])
    |> validate_required([:site, :client_id, :client_secret])
    |> unique_constraint(:site)
  end
end
