defmodule ActivityPubClient.Chat do
  @moduledoc """
  Requests to the chat API in Pleroma.

  Reference: https://docs-develop.pleroma.social/backend/development/API/chats/
  """

  @doc """
  Creates or gets an existing Chat for a certain recipient (identified by Account ID).

  Use `ActivityPubClient.Search.search/3` to get a recipent ID.
  """
  def get_chat(client, account_id) do
    case OAuth2.Client.post(client, "/api/v1/pleroma/chats/by-account-id/#{account_id}") do
      {:ok, %OAuth2.Response{body: resp}} ->
        {:ok, resp}

      {:error, %OAuth2.Response{status_code: 401, body: _body}} ->
        {:error, :unauthorized}

      {:error, %OAuth2.Error{reason: reason}} ->
        {:error, inspect(reason)}
    end
  end

  @doc """
  Posts a message to a chat.

  Expects a client, a chat_id returned by `get_chat/2`, the message content, and an optional media ID.
  """
  def post_message(client, chat_id, content, media_id \\ nil) do
    body =
      if media_id do
        %{
          content: content,
          media_id: media_id
        }
      else
        %{
          content: content
        }
      end

    case OAuth2.Client.post(client, "/api/v1/pleroma/chats/#{chat_id}/messages", body, [
           {"Content-Type", "application/json"}
         ]) do
      {:ok, %OAuth2.Response{body: resp}} ->
        {:ok, resp}

      {:error, %OAuth2.Response{status_code: 401, body: _body}} ->
        {:error, :unauthorized}

      {:error, %OAuth2.Error{reason: reason}} ->
        {:error, inspect(reason)}
    end
  end
end
